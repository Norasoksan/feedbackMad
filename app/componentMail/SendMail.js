import React from 'react'
import axios from 'axios'
import { Alert } from 'react-native'


const sendMail = (feedback) =>{
    return new Promise((resolve,reject)=>{
        let body = {
            personalizations: [
                {
                    to: [{ email: 'manny@workwithmad.com' }, 
                         { email: 'kit@workwithmad.com' } ,
                         { email: 'erika@workwithmad.com' },
                         { email: 'parker@workwithmad.com' },
                         { email: 'uysimty@workwithmad.com' }
                         ]
                    // to: [{ email: 'soksanchhom37@gmail.com' }]
                }],
            from: { email: 'feedback@mad.com' },
            subject: 'Feedback',
            content: [{ type: "text/html", value: feedback }]
        };

        const API = 'https://api.sendgrid.com/v3/mail/send';

        const config = {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer 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'
                // 'Authorization': 'Bearer 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'
            }
        }

        axios.post(API, JSON.stringify(body), config)
            .then(response => {
                resolve(response);
            })
            .catch(err => {
                console.log('Error request: ', err.response);
                reject(err);
            })
    })

}
export default sendMail;