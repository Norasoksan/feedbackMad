
const mailText = (data) => {
    return `<div style="font-family:'arial,sans-serif';font-size:17px">
            <p>Dear Mad Team,</p>
            <p>I am ${data.user}. I have rated your service as below :</p>
            <p>* Quality : ${data.quality}</p>
            <p>* Speed : ${data.speed}</p>
            <p>* Value : ${data.value}</p>
            <p>* Creativity : ${data.creativity}</p>
            <p>* Strategy : ${data.strategy}</p>
            <pre style="font-family:'arial,sans-serif';font-size:17px">${data.comment}</pre>
            </br>
            </div>`
}

export default mailText;