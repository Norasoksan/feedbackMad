import React from 'react'
import { Text, StyleSheet, View } from 'react-native'
import Label from '../componentHeader/Label'
import StarRating from 'react-native-star-rating';


export default class Star extends React.Component{
    render(){
        return(
            <View style={styles.viewRatedStyle}>
                <Label title={this.props.starLabel} />
                <StarRating
                    buttonStyle={styles.startRateStyle}
                    starSize={35}
                    disabled={false}
                    maxStars={5}
                    rating={this.props.categoryStar}
                    starColor='#EACD2F'
                    selectedStar={this.props._onSelectedStar}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewRatedStyle: {
        marginTop: 10,
        alignItems: 'center',
        padding: 20,
        borderTopWidth: 1,
        borderColor: '#A1887F',
        // backgroundColor : '#e8e8e8',
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    startRateStyle: {
        marginHorizontal: 5,

    },
})