import React from 'react'
import { TouchableOpacity, View,StyleSheet} from 'react-native'

export default class MailButton extends React.Component{
    render(){
        return(
            <View style={styles.viewButton}>
                <TouchableOpacity style={styles.button} 
                                  onPress={this.props._onPress}
                                  disabled={this.props._disabled} >
                    {this.props.buttonLabel}
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    viewButton: {
        marginTop: 10
    },
    button: {
        width: '100%',
        backgroundColor: '#e3211a',
        padding: 15,
        alignItems: 'center',
        marginBottom: 10,
    },

})
