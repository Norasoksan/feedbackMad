import React from 'react';
import Toast from 'react-native-root-toast';

export default class ValidateMessage extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            message: this.props.message,
            visible: this.props.visible,
            bgcolor: this.props.bgcolor
            
        };
    }
    componentWillReceiveProps(nextProps) {
        if (this.state.message != nextProps.message) {
            this.setStateValue('message', nextProps.message);
        }
        if (this.state.visible != nextProps.visible) {
            this.setStateValue('visible', nextProps.visible);
        }
        if (this.state.bgcolor != nextProps.bgcolor) {
            this.setStateValue('bgcolor', nextProps.bgcolor);
        }
    }

    setStateValue(name, value) {
        if ( name == 'message' ) this.setState({ message: value });
        if ( name == 'visible' ) this.setState({ visible: value });
        if ( name == 'bgcolor' ) this.setState({ bgcolor: value });
        
    }
    render() {
        return (
            <Toast
                accessibilityLabel="validate"
                position={550}
                animation={true}
                textColor='#fff'
                hideOnPress={false}
                visible={this.state.visible}
                backgroundColor={this.state.bgcolor}>
                {this.state.message}
            </Toast>
        )
    }
}

