import React from 'react'
import {View, Text, StyleSheet } from 'react-native'

export default class Label extends React.Component{
    render(){
        return(
            <View style={styles.viewStyle}>
                <Text style={styles.textUser}>{this.props.title}</Text>
                {this.props.required? 
                    <Text style={{color:'red'}}>*</Text> 
                : null 
                }
            </View>
            
        )
    }
}

const styles = StyleSheet.create({
    viewStyle:{
       flexDirection : 'row'
    },
    textUser: {
        fontSize: 16,
        fontWeight: 'bold',
    },
})