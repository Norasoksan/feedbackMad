import React from 'react'
import {View, Image,StyleSheet} from 'react-native'

export default class Logo extends React.Component{
    render(){
        return(
            <View style={styles.imgViewstyle}>
                <Image
                    style={styles.imgStyle}
                    source={this.props.src}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    imgViewstyle: {
        alignItems: 'center',
    },
    imgStyle: {
        width: 150,
        height: 70,
    },
})
