import React from 'react';
import { StyleSheet,
        Text, 
        View, 
        TouchableOpacity, 
        TextInput,Button, 
        ScrollView,
        Image,
        KeyboardAvoidingView,
        ToastAndroid,
        Alert, 
        ActivityIndicator } from 'react-native';

import Header from './app/componentHeader/Header';
import Logo from './app/componentHeader/Logo';
import Label from './app/componentHeader/Label'
import ValidateMessage from './app/componentBody/ValidateMessage'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Star from './app/componentBody/Star'
import SendMail from './app/componentMail/SendMail'
import MailText from './app/componentMail/MailText'
import MailButton from './app/componentBody/MailButton'
import axios from 'axios'




export default class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
          user: "",
          quality: 0,
          speed: 0,
          value: 0,
          creativity: 0,
          strategy: 0,
          comment: "",
          validateMsg: '',
          validatebg : '',
          validateVisible : false,
          loading : false
    }
  }

  //check label when user button is selected
  renderLabel() {
    if (this.state.user === 'client') {
      return <Label title={'I am Client'} />
    } else if (this.state.user === 'employee') {
      return <Label title={'I am Employee'} />
    } else {
      return <Label title={'Who are you?'} required={true} />
    }
  }

  //check user data to show action button
  renderUserButton(){
    if(this.state.user === 'client'){

      return <View style={styles.viewTabStyle}>
        <TouchableOpacity 
          style={styles.tabClickStyle} 
          onPress={() => this.onSelectUser('client')}>
          <Text style={styles.textUserClick}>{'Client'}</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.tabStyle} 
          onPress={() => this.onSelectUser('employee')}>
          <Text>{'Employee'}</Text>
        </TouchableOpacity>
      </View>

    }else if(this.state.user === 'employee'){

      return <View style={styles.viewTabStyle}>
        <TouchableOpacity 
          style={styles.tabStyle} 
          onPress={() => this.onSelectUser('client')}>
          <Text>{'Client'}</Text>
        </TouchableOpacity>
        <TouchableOpacity 
          style={styles.tabClickStyle} 
          onPress={() => this.onSelectUser('employee')}>
          <Text style={styles.textUserClick}>{'Employee'}</Text>
        </TouchableOpacity>
      </View>

    }else{
      return <View style={styles.viewTabStyle}>
        <TouchableOpacity 
          style={styles.tabStyle} 
          onPress={() => this.onSelectUser('client')}>
          <Text>{'Client'}</Text>
        </TouchableOpacity>
        <TouchableOpacity 
          style={styles.tabStyle} 
          onPress={() => this.onSelectUser('employee')}>
          <Text>{'Employee'}</Text>
        </TouchableOpacity>
      </View>
    }
  }

  //select user function
  onSelectUser = (val) => {
    this.setState({
      user : val
    })
  }

  //set star rating by category
  onStarRatingPress(rating,category) {
    console.log
    switch (category){
      case 'quality': 
        this.setState({
          quality: rating
          })
        break;
      case 'speed':
        this.setState({
          speed: rating
        })
        break;
      case 'value':
        this.setState({
          value: rating
        })
        break;
      case 'creativity':
        this.setState({
          creativity: rating
        })
        break;
      case 'strategy':
        this.setState({
          strategy: rating
        })
        break;
      deafult:
        console.log('Nothing Changed');
    }
  }

  //set comment value
  onChangeMessageHandler = (val) => {
    this.setState({
      comment : val
    })
  }

  //exchange MailButton label by loading
  renderMailButton(){
    if(this.state.loading === true){
      return <MailButton
        _onPress={this.onSubmit}
        _disabled = {this.state.loading}
        buttonLabel={
          <ActivityIndicator
            animating={this.state.loading}
            size="small"
            color="white" />
        }
      />

    }else{
      return <MailButton
        _onPress={this.onSubmit}
        _disabled={this.state.loading}
        buttonLabel={
          <Text style={styles.textButton}>{'Send Message'}</Text>
        }
      />

    }
  }

  //hide validate toast
  hideValidate() {
    setTimeout(() => {
      this.setState({ validateVisible: false })
    }, 4000);
  }


  //send mail to server
  onSubmit = () => {
    if (this.state.user === '') {
      this.setState({
        validateMsg: 'Please choose option on Who you are',
        validatebg: 'red',
        validateVisible: true
      })
      this.hideValidate();
    } else {
      this.setState({
        loading : true
      })
      const { user, quality, speed, value, creativity, strategy, comment } = this.state;
      let feedback = MailText({ user, quality, speed, value, creativity, strategy, comment });
      SendMail(feedback)
        .then((response) => {
          if (response.status >= 200 && response.status <= 300) {
            this.setState({
              user: "",
              quality: 0,
              speed: 0,
              value: 0,
              creativity: 0,
              strategy: 0,
              comment: "",
              validateMsg: 'Thanks,Feedback has been sent to our team successfully',
              validatebg: 'green',
              validateVisible: true,
              loading : false
            });
            this.hideValidate();
            console.log(response);
          }
          else {
            this.setState({
              loading: false
            })
            Alert.alert(
              'Please try again later',
              response.status,
              [
                { text: 'OK' }
              ]);
          }
        })
        .catch((err) => {
          this.setState({
            loading : false
          })
          Alert.alert(
            err.message,
            err.message === 'Network Error' ? 'Please Check you Connection and try again later': null,
            [
              { text: 'OK' }
            ]);
          console.log('err message',err.message);
        });
    }

  }

  render() {
    return (
      
      <View style={styles.container}>
        <Header headerText='Feedback Mad' />

        {/* Validation */}
        <ValidateMessage bgcolor={this.state.validatebg} visible={this.state.validateVisible} message={this.state.validateMsg} />

        <KeyboardAwareScrollView enableResetScrollToCoords={false}>
          <View>
            <ScrollView>

              <View style={styles.innerContainer}>

                <Logo src={require('./app/img/mad.png')} />
                
                <View style={styles.viewUserTextStyle}>
                  {this.renderLabel()}
                </View>

                {/* User Button */}
                {this.renderUserButton()}

                <View style={styles.viewUserTextStyle}>
                    <Label title={'Tab To Rate Our Service'}/>
                </View>

                {/* Star Rating by category */}
                <Star starLabel={'Quality'}
                      categoryStar={this.state.quality}
                      _onSelectedStar = {(rating) => this.onStarRatingPress(rating,'quality')}
                />

                <Star starLabel={'Speed'}
                      categoryStar={this.state.speed}
                      _onSelectedStar={(rating) => this.onStarRatingPress(rating, 'speed')}
                />

                <Star starLabel={'Value'}
                      categoryStar={this.state.value}
                      _onSelectedStar={(rating) => this.onStarRatingPress(rating, 'value')}
                />

                <Star starLabel={'Creativity'}
                      categoryStar={this.state.creativity}
                      _onSelectedStar={(rating) => this.onStarRatingPress(rating, 'creativity')}
                />

                <Star starLabel={'Strategy'}
                      categoryStar={this.state.strategy}
                      _onSelectedStar={(rating) => this.onStarRatingPress(rating, 'strategy')}
                />

                {/* Comment Input */}
                <View style={styles.viewInputMessage}>
                    <TextInput
                    autoFocus={false}
                    onChangeText={this.onChangeMessageHandler}
                    style={styles.inputMessageStyle}
                    multiline={true}
                    placeholder="Comment us here..."
                    value={this.state.comment}
                  />
                </View>

                {/* Send Button */}
                {this.renderMailButton()}

              </View>
              
            </ScrollView>
          </View>
        </KeyboardAwareScrollView>
      </View>
      
    );
  }
}



const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'flex-start',
    
  },
  innerContainer : {
    margin: 15,
    shadowColor: '#000',
    shadowRadius : 10,
    shadowOpacity: 0.3,
    padding : 10
  },
  viewUserTextStyle : {
    marginTop: 20,
    alignItems : 'center'
    
  },
  viewTabStyle: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop : 10,
    
  },
  tabStyle : {
    alignItems: 'center',
    backgroundColor: '#ECEFF1',
    width : '45%',
    padding : 20,
    borderColor: '#e3211a',
    borderWidth : 1,
  },
  tabClickStyle : {
    alignItems: 'center',
    backgroundColor: '#ea4a44',
    width: '45%',
    padding: 20,
    borderColor: '#e3211a',
    borderWidth: 1,
  },
  textUserClick: {
    fontSize: 15,
    color : '#FFF',
    
  },
  viewInputMessage : {
    marginTop: 0,
    borderTopWidth: 1,
    borderColor: '#A1887F',
    paddingTop: 10,

  },
  inputMessageStyle:{
    borderWidth : 1,
    borderColor : '#000',
    height : 100,
    padding : 10,
    fontSize : 15,

  },
  textButton : {
    color: '#FFF',
  },
});
